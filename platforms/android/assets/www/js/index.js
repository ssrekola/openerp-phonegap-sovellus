//Saattaa jättää buttonit maalatuiksi.
$(document).bind("mobileinit", function() {
    $.mobile.defaultDialogTransition = "none";
    $.mobile.defaultPageTransition = "none";

});

$(function () {
    FastClick.attach(document.body);

    /*username checks*/
    var kayttaja = $("#loginUsername");
    kayttaja.blur(function () { User(); });
    var passu = $("#loginPassword");
    passu.blur(function () { pWord(); });

    /* swipe panel for settings */
    $(document).on("swipeleft swiperight", "#loginView", function (e) {
        //
            if (e.type === "swipeleft") {

                $("#mypanel").panel("close");
                $(".asetuksetButtoniDiv").html("<button id='bAsetusPanel' data-theme='b' data-icon='chevron-right'>Asetukset</button>")
                $("#bAsetusPanel").button();
            //check if panel is already open to prevent "jumping" behavior
            } else if (($.mobile.activePage.jqmData("panel") !== "open") && e.type === "swiperight") {
                $("#mypanel").panel("open");
                $(".asetuksetButtoniDiv").html("<button id='bAsetusPanel' data-theme='b' data-icon='chevron-left'>Asetukset</button>")
                $("#bAsetusPanel").button();
                checkIfSaved();
            }    });
    // change settings button text on click
    $(document).on("click", "#bAsetusPanel", function (e){
        if (e.type === "click"){
           $(".asetuksetButtoniDiv").html("<button id='bAsetusPanel' data-theme='b' data-icon='chevron-right' data-iconpos='right'>Swipe me!</button>");
           $("#bAsetusPanel").button();
        }
    });

});

/* page transition bound functions */
$(document).delegate("#loginView","pageshow", function() { cbTarkistus(); });

/* universal variables for local storage handling, see: window.storage  */
var savedURL;
var savedPORT;
var savedDB;
var savedUser;
var cbState; // [true|false], only for the checkbox

/*
 * Viivakoodin luku
 */

var barcode;

function readBarcode() {

    var scanner = cordova.require("cordova/plugin/BarcodeScanner");
    scanner
            .scan(
            function(result) {
                barcode = result.text;
                var confirmation = confirm("Hyväksy viivakoodi painamalla OK \n"
                        + barcode);

                if (confirmation === true) {
                    window.location = "#receiveDeliveryView";
                    $("#barcodeResult").text(barcode);
                    $('#products').empty();
                    getOrderID();
                }
            },
            function(error) {
                alert("Lukeminen epäonnistui: " + error);
            });
}

/*
 * OpenERP Kilkkeet alla
 */

/*
* alustetaan tietojen tallennus localstorageen
* methods: storage.set [key, value] && storage.get [key] && storage.remove [key]
*/

window.storage = {
    saveIn:localStorage,
    get: function( key ) {
        try {
            return JSON.parse(this.saveIn[key]);
        } catch(e) {};
        return undefined;
    },
    set: function( key, value) {
        try {
            this.saveIn[key] = JSON.stringify(value);
        } catch(e) {};
    },
    remove: function( key ) {
        try{
    localStorage.removeItem( key );
    } catch(e){};
}
};


/* Save server info into localStorage */

function saveSettings() {
    //location = "#settingsView";
    var sURL = document.getElementById("SETTINGS_serverURL").value;
    var sPORT = document.getElementById("SETTINGS_serverPORT").value;
    var sDB = document.getElementById("SETTINGS_serverDB").value;
        storage.set("serverURL", sURL);
        storage.set("serverPORT", sPORT);
        storage.set("serverDB", sDB);
    //debug: console.log("tallennus");
    window.location.reload();
    //checkIfSaved();
        }
/* TODO: aika puukotus, mutta jos haluaa että menu aukeaa uudestaan tallennuksen jälkeen.. tai ennen tallennusta :

function savedRefresh(){
    if (typeof savedURL !== "undefined" && savedURL !== ""){
    checkIfSaved();
    $("#mypanel").panel("open");
    $("#mypanel").trigger("updatelayout");
    $(".asetuksetButtoniDiv").html("<button id='bAsetusPanel' data-theme='b' data-icon='chevron-left'>Asetukset</button>")
    $("#bAsetusPanel").button(); }

}
*/

function checkIfSaved(){

    /*  checkIfSaved()
     *  tayttaa kentat valmiiksi mikali localstoragesta loytyy
     *  tarkistus tehdään nyt ++ incrementillä koska true|false ei saatu toimimaan
     */

    var toThree = 0; // count for times of success
         if (typeof savedURL !== "undefined" && savedURL !== ""){
        document.getElementById("SETTINGS_serverURL").value=savedURL;

        toThree++;
        //debug:
        console.log(toThree);
        urlOK();
        }else{return false;}

        function urlOK(){
        if (typeof savedPORT !== "undefined" && savedPORT !== "" ){
        document.getElementById("SETTINGS_serverPORT").value=savedPORT;
         toThree++;
         //debug:
         console.log(toThree);
        portOK();
        }else{return false;}}

        function portOK(){
        if (savedDB !== "undefined" && savedDB !== "" ){
        document.getElementById("SETTINGS_serverDB").value=savedDB;

        toThree++;
        //debug:
        console.log(toThree);
        onkoKaikkiHyvin();
        }else{return false;}}

        function onkoKaikkiHyvin(){
           if (toThree === 3){
           console.log("kaikki on hyvin");
           $("#asetuksetOkDiv").html("<div id='asetuksetTallennettuDiv' data-theme='b' class='message success'><i class='icon-ok'></i><p>Tiedot on tallennettu.</p></div>")
           $("#mypanel").trigger("updatelayout");
           }
        else if (toThree === 0){console.log("Tietoja ei tallennettu.");}
    }
}

// universal variables for local storage url handling
    savedURL = storage.get("serverURL");
    savedPORT = storage.get("serverPORT");
    savedDB = storage.get("serverDB");
    savedUser = storage.get("savedUser");
    cbState = storage.get("cbState"); // [true|false], tarkistaa yritetaanko localstoragesta username hakua

/*
 * cbClick()
 * checkbox value [true|false]
 * kutsutaan onclickillä, tallentaa & poistaa localstoragesta tiedon valittomasti painettaessa
 */

function cbClick(){
var $cbState = $("#cbUser");
    if ($cbState.prop('checked') == true){
      cbState = true;
      username = document.getElementById('loginUsername').value;
      storage.set("savedUser", username);
      storage.set("cbState",cbState);
      //debug: console.log(username);
    }
    if ($cbState.prop('checked') == false){
      cbState = false;
      storage.remove("savedUser");
      storage.set("cbState",cbState);
   //delete username
    }
    //debug: console.log($cbState.prop('checked'));
}

function User(){
    username = document.getElementById('loginUsername').value;
    //debug: console.log("jquery blur", username);
   }
function pWord(){
    pwd = document.getElementById('loginPassword').value;
    //debug: console.log("jquery pw", pwd);
}
/* cbTarkistus()
 * tarkistetaan localstoragesta checkboxin arvo [true|false] ja palautetaan sen mukaisesti arvot
 */


function cbTarkistus(){
    if (cbState){
        document.getElementById("loginUsername").value=savedUser;
        $("#cbUser").prop("checked",true).checkboxradio("refresh");
        //debug: console.log(cbState);
    }else{
        $("#cbUser").prop("checked",false).checkboxradio("refresh");}
}


/* XML-RPC connection settings */
var server = savedURL+":"+savedPORT+"/xmlrpc/"; // server = "http://dllopenerp.cloudapp.net:80/xmlrpc/";

var username;
var pwd; //"Hamkkissa2";
var dbname = savedDB; //"HamkdevDB";
var uid;
var orderID;

function login() {
    if (storage.get("savedUser") !== undefined) {
        username = savedUser;
    } else if (username !== undefined){
        username;
    }


    var request = new XmlRpcRequest(server + "common", "login");
    request.addParam(dbname);
    request.addParam(username);
    request.addParam(pwd);
    var response = request.send();
    uid = response.parseXML();
    if (uid !== false) {
        //debug
        console.log(uid);
        location = "#menuView";
    }else{
        //debug
       console.log("error; " + uid);}
    $( ".errorLink").trigger('click');
}

function getOrderID() {
    var args = [["name", "=", barcode]];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.picking");
    request.addParam("search");
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    getSupplier(responseXML[0]);
    getDeliveryTime(responseXML[0]);
    getPurchaseID(responseXML[0]);
    orderID = responseXML[0];
}

function getSupplier(id) {
    var args = ["partner_id"];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.picking");
    request.addParam("read");
    request.addParam([id]);
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    $("#supplier").text(responseXML[0]["partner_id"][1]);
}
function getDeliveryTime(id) {
    var args = ["min_date"];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.picking");
    request.addParam("read");
    request.addParam([id]);
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    $("#deliveryTime").text(responseXML[0]["min_date"].slice(0, -8));
}

function getPurchaseID(id) {
    var args = ["purchase_id"];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.picking");
    request.addParam("read");
    request.addParam([id]);
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    getProductID(responseXML[0]["purchase_id"][0]);
}

function getProductID(id) {
    var args = [["order_id", "=", id]];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("purchase.order.line");
    request.addParam("search");
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    getProducts(responseXML);
}

function getProducts(id) {
    var arraylength = id.length;
    for (i = 0; i < arraylength; i++) {
        var args = ["id", "name", "product_qty", "product_id"];
        var request = new XmlRpcRequest(
                server + "object", "execute");
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("purchase.order.line");
        request.addParam("read");
        request.addParam([id[i]]);
        request.addParam(args);
        var response = request.send();
        var responseXML = response.parseXML();

        request = new XmlRpcRequest(
        server + "object", "execute");
        var args = [["purchase_line_id", "=", responseXML[0]["id"]]];
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.move");
        request.addParam("search");
        request.addParam(args);
        var stockMoveidResponse = request.send();
        var stockMoveid = stockMoveidResponse.parseXML();
        
        $("#products").append("<h3 onclick=receiveProduct(" + stockMoveid[0] + ");>" +
                responseXML[0]["name"] + "</h3>" + "<p>Määrä: "
                + responseXML[0]["product_qty"] + "</p>");
        getProductTemplateID(responseXML[0]["product_id"][0]);
    }
}

function getProductTemplateID(id) {
    var args = ["product_tmpl_id"];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("product.product");
    request.addParam("read");
    request.addParam([id]);
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    getProductShelfLocation(responseXML[0]["id"]);
}

function getProductShelfLocation(id) {
    var args = ["loc_rack", "loc_row", "loc_case"];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("product.template");
    request.addParam("read");
    request.addParam([id]);
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();
    $("#products").append("<p>Hylly: " + responseXML[0]["loc_rack"] + "</p>" +
            "<p>Rivi: " + responseXML[0]["loc_row"] + "</p>" + "<p>Lokero: " + responseXML[0]["loc_case"] + "<p>");
}

function receiveDelivery() {
    var confirmation = confirm("Kuitataanko tilaus ja tuotteet vastaanotetuiksi?");
    if (confirmation === true)
    {
        var request = new XmlRpcRequest(
                server + "object", "exec_workflow");
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.picking");
        request.addParam("button_done");
        request.addParam(orderID);
        request.send();
        alert("Tilaus kuitattu");
        listOrders();
        listProducts();
        window.location = "#receiveDeliveryMenuView";
    }
}

function listOrders() {
    $('#orderList').empty();
    var args = [["state", "=", "assigned"], ["type", "=", "in"]];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.picking");
    request.addParam("search");
    request.addParam(args);
    var response = request.send();
    var id = response.parseXML();
    var arraylength = id.length;
    for (i = 0; i < arraylength; i++) {
        args = ["name"];
        request = new XmlRpcRequest(
                server + "object", "execute");
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.picking");
        request.addParam("read");
        request.addParam([id[i]]);
        request.addParam(args);
        response = request.send();
        var responseXML = response.parseXML();
        $("#orderList").append("<button data-theme='b' onclick=getOrder(" + [id[i]] + "," + "'" +
                responseXML[0]["name"] + "'" + ");>" + responseXML[0]["name"] + "</button>");
        $(":button").button(); // CSS button styling
    }

}

function getOrder(id, order) {
    window.location = "#receiveDeliveryView";
    $("#barcodeResult").text(order);
    $('#products').empty();
    getSupplier(id);
    getDeliveryTime(id);
    getPurchaseID(id);
    orderID = id;
}
 // listaa tuotteet iralleen tavaran vastaanotto ruudulle

function listProducts() {
    $('#productList').empty();
    var args = [["state", "=", "assigned"], ["purchase_line_id", "!=", "null"]];
    var request = new XmlRpcRequest(
            server + "object", "execute");
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.move");
    request.addParam("search");
    request.addParam(args);
    var response = request.send();
    var id = response.parseXML();
    var arraylength = id.length;
    for (i = 0; i < arraylength; i++) {
        args = ["name", "product_qty"];
        request = new XmlRpcRequest(
                server + "object", "execute");
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.move");
        request.addParam("read");
        request.addParam([id[i]]);
        request.addParam(args);
        response = request.send();
        var responseXML = response.parseXML();
        $("#productList").append("<h2 onclick=receiveProduct(" + [id[i]] + ");>" + responseXML[0]["name"] + "</h2>\n\
            <p>Määrä: " + responseXML[0]["product_qty"] + "</p>");
    }
}

// TODO: Ei merkkaa itse tilausta vastaanotetuksi vaikka kaikki tilauksen tuotteet olisi. Korjaa myöhemmin workflowlla?
function receiveProduct(id) {
    var confirmation = confirm("Vastaanota tuote painamalla OK");
    if (confirmation === true)
    {
        var args = {
            "state": "done"
        };
        var request = new XmlRpcRequest(
                server + "object", "execute");
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.move");
        request.addParam("write");
        request.addParam(id);
        request.addParam(args);
        request.send();
        alert("Tuote vastaanotettu");
        listProducts();
    }
}

function receiveProducts() {
    var request = new XmlRpcRequest(
            server + "object", "execute");
    var args = [["picking_id", "=", orderID]];
    request.addParam(dbname);
    request.addParam(uid);
    request.addParam(pwd);
    request.addParam("stock.move");
    request.addParam("search");
    request.addParam(args);
    var response = request.send();
    var responseXML = response.parseXML();

    request.clearParams();

    args = {
        "state": "done"
    };
    var arraylength = responseXML.length;
    for (i = 0; i < arraylength; i++) {

        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.move");
        request.addParam("write");
        request.addParam([responseXML[i]]);
        request.addParam(args);
        request.send();
        request.clearParams();
    }
    alert("Tilaus vastaanotettu.");
}

/*
//Tuotteen osittainen vastaanottaminen. Selvitä annettavat argumentit.
function testi(){
        var request = new XmlRpcRequest(
            server + "object", "execute");
        var args = {
                    "product_id":"40",
                    "product_qty":"2"
        };
        request.addParam(dbname);
        request.addParam(uid);
        request.addParam(pwd);
        request.addParam("stock.move");
        request.addParam("do_partial");
        request.addParam([64]);
        request.addParam(args);
        var response = request.send();
        var responseXML = response.parseXML();
        console.log(responseXML["faultCode"]);
        console.log(responseXML["faultString"]);
        console.log(responseXML["type"]);
        console.log("läpi");
}

 function getDatabases() {
 var request = new
 XmlRpcRequest(server + "db", "list");
 var response = request.send();
 alert(response.parseXML());
 //$("#databases").text(response.parseXML());
 }
 
 function getDestinationWarehouse(id) {
 var args = ["warehouse_id"];
 var request = new XmlRpcRequest(
 server + "object", "execute");
 request.addParam(dbname);
 request.addParam(uid);
 request.addParam(pwd);
 request.addParam("purchase.order");
 request.addParam("read");
 request.addParam([id]);
 request.addParam(args);
 var response = request.send();
 var responseXML = response.parseXML();
 $("#warehouse").text(responseXML[0]["warehouse_id"][1]);
 }
 
 //Tilauksen vastaanottaminen suoraan tietokantaan kirjoittamalla.
 function receiveDelivery() {
 var args = {
 "state": "done"
 };
 var request = new XmlRpcRequest(
 server + "object", "execute");
 request.addParam(dbname);
 request.addParam(uid);
 request.addParam(pwd);
 request.addParam("stock.picking");
 request.addParam("write");
 request.addParam([orderID]);
 request.addParam(args);
 request.send();
 
 request.clearParams();
 
 args = [["picking_id", "=", orderID]];
 request.addParam(dbname);
 request.addParam(uid);
 request.addParam(pwd);
 request.addParam("stock.move");
 request.addParam("search");
 request.addParam(args);
 var response = request.send();
 var responseXML = response.parseXML();
 
 request.clearParams();
 
 args = {
 "state": "done"
 };
 var arraylength = responseXML.length;
 for (i = 0; i < arraylength; i++) {
 
 request.addParam(dbname);
 request.addParam(uid);
 request.addParam(pwd);
 request.addParam("stock.move");
 request.addParam("write");
 request.addParam([responseXML[i]]);
 request.addParam(args);
 request.send();
 request.clearParams();
 }
 alert("Tilaus vastaanotettu.");
 }
 */